<?php namespace C4\FrontendBundle\Models;

/**
 * Class User
 * @package C4\FrontendBundle\Models
 */
class User
{
    const ROLE_GUEST = "guest";
    const ROLE_USER = "user";
    const ROLE_ADMIN = "admin";
    const ROLE_MANAGER = "manager";
    const ROLE_STATISTICS = "statistics";

    public $role = "guest";

    public $id = null;
    public $email = null;
    public $password = null;
    public $first_name = null;
    public $last_name = null;
    public $phone = null;

}
