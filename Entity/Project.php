<?php

namespace C4\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Locale;

/**
 * Project
 *
 * @ORM\Table(name="projects")
 * @ORM\Entity(repositoryClass="C4\FrontendBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title_lv", type="string", length=255, nullable=true)
     */
    private $titleLv;

    /**
     * @var string
     *
     * @ORM\Column(name="title_ru", type="string", length=255, nullable=true)
     */
    private $titleRu;

    /**
     * @var string
     *
     * @ORM\Column(name="description_lv", type="string", length=255, nullable=true)
     */
    private $descriptionLv;

    /**
     * @var string
     *
     * @ORM\Column(name="description_ru", type="string", length=255, nullable=true)
     */
    private $descriptionRu;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTitle(){
        return $this->{"getTitle".ucfirst(app()->getLocale())}();
    }
    public function setTitle($title){

        return $this->{"setTitle".ucfirst(app()->getLocale())}($title);
    }
    public function getDescription(){
        return $this->{"getDescription".ucfirst(app()->getLocale())}();
    }
    public function setDescription($description){
        return $this->{"setDescription".ucfirst(app()->getLocale())}($description);
    }

    /**
     * Set titleLv
     *
     * @param string $titleLv
     *
     * @return Project
     */
    public function setTitleLv($titleLv)
    {
        $this->titleLv = $titleLv;

        return $this;
    }

    /**
     * Get titleLv
     *
     * @return string
     */
    public function getTitleLv()
    {
        return $this->titleLv;
    }

    /**
     * Set titleRu
     *
     * @param string $titleRu
     *
     * @return Project
     */
    public function setTitleRu($titleRu)
    {
        $this->titleRu = $titleRu;

        return $this;
    }

    /**
     * Get titleRu
     *
     * @return string
     */
    public function getTitleRu()
    {
        return $this->titleRu;
    }

    /**
     * Set descriptionLv
     *
     * @param string $descriptionLv
     *
     * @return Project
     */
    public function setDescriptionLv($descriptionLv)
    {
        $this->descriptionLv = $descriptionLv;

        return $this;
    }

    /**
     * Get descriptionLv
     *
     * @return string
     */
    public function getDescriptionLv()
    {
        return $this->descriptionLv;
    }

    /**
     * Set descriptionRu
     *
     * @param string $descriptionRu
     *
     * @return Project
     */
    public function setDescriptionRu($descriptionRu)
    {
        $this->descriptionRu = $descriptionRu;

        return $this;
    }

    /**
     * Get descriptionRu
     *
     * @return string
     */
    public function getDescriptionRu()
    {
        return $this->descriptionRu;
    }
}
