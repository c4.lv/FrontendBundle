<?php

namespace C4\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Locale;

/**
 * SurveyItem
 *
 * @ORM\Table(name="survey_items")
 * @ORM\Entity(repositoryClass="C4\FrontendBundle\Repository\SurveyItemRepository")
 */
class SurveyItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer")
     */
    private $parentId;

    /**
     * @var int
     *
     * @ORM\Column(name="npk", type="integer")
     */
    private $npk;

    /**
     * @var int
     *
     * @ORM\Column(name="survey_id", type="integer")
     */
    private $surveyId;


    /**
     * @var string
     *
     * @ORM\Column(name="title_lv", type="string", length=255, nullable=true)
     */
    private $titleLv;

    /**
     * @var string
     *
     * @ORM\Column(name="title_ru", type="string", length=255, nullable=true)
     */
    private $titleRu;

    /**
     * @var string
     *
     * @ORM\Column(name="description_lv", type="string", length=255, nullable=true)
     */
    private $descriptionLv;

    /**
     * @var string
     *
     * @ORM\Column(name="description_ru", type="string", length=255, nullable=true)
     */
    private $descriptionRu;

    /**
     * @var string
     *
     * @ORM\Column(name="answer_type", type="string", length=100, nullable=true)
     */
    private $answerType;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="long_value", type="text", nullable=true)
     */
    private $longValue;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    public function getTitle(){
        return $this->{"getTitle".ucfirst(app()->getLocale())}();
    }
    public function setTitle($title){

        return $this->{"setTitle".ucfirst(app()->getLocale())}($title);
    }
    public function getDescription(){
        return $this->{"getDescription".ucfirst(app()->getLocale())}();
    }
    public function setDescription($description){
        return $this->{"setDescription".ucfirst(app()->getLocale())}($description);
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return SurveyItem
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set npk
     *
     * @param integer $npk
     *
     * @return SurveyItem
     */
    public function setNpk($npk)
    {
        $this->npk = $npk;

        return $this;
    }

    /**
     * Get npk
     *
     * @return integer
     */
    public function getNpk()
    {
        return $this->npk;
    }

    /**
     * Set surveyId
     *
     * @param integer $surveyId
     *
     * @return SurveyItem
     */
    public function setSurveyId($surveyId)
    {
        $this->surveyId = $surveyId;

        return $this;
    }

    /**
     * Get surveyId
     *
     * @return integer
     */
    public function getSurveyIdId()
    {
        return $this->surveyId;
    }

    /**
     * Set titleLv
     *
     * @param string $titleLv
     *
     * @return SurveyItem
     */
    public function setTitleLv($titleLv)
    {
        $this->titleLv = $titleLv;

        return $this;
    }

    /**
     * Get titleLv
     *
     * @return string
     */
    public function getTitleLv()
    {
        return $this->titleLv;
    }

    /**
     * Set titleRu
     *
     * @param string $titleRu
     *
     * @return SurveyItem
     */
    public function setTitleRu($titleRu)
    {
        $this->titleRu = $titleRu;

        return $this;
    }

    /**
     * Get titleRu
     *
     * @return string
     */
    public function getTitleRu()
    {
        return $this->titleRu;
    }

    /**
     * Set descriptionLv
     *
     * @param string $descriptionLv
     *
     * @return SurveyItem
     */
    public function setDescriptionLv($descriptionLv)
    {
        $this->descriptionLv = $descriptionLv;

        return $this;
    }

    /**
     * Get descriptionLv
     *
     * @return string
     */
    public function getDescriptionLv()
    {
        return $this->descriptionLv;
    }

    /**
     * Set descriptionRu
     *
     * @param string $descriptionRu
     *
     * @return SurveyItem
     */
    public function setDescriptionRu($descriptionRu)
    {
        $this->descriptionRu = $descriptionRu;

        return $this;
    }

    /**
     * Get descriptionRu
     *
     * @return string
     */
    public function getDescriptionRu()
    {
        return $this->descriptionRu;
    }

    /**
     * Set answerType
     *
     * @param string $answerType
     *
     * @return SurveyItem
     */
    public function setAnswerType($answerType)
    {
        $this->answerType = $answerType;

        return $this;
    }

    /**
     * Get answerType
     *
     * @return string
     */
    public function getAnswerType()
    {
        return $this->answerType;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return SurveyItem
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return SurveyItem
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set longValue
     *
     * @param string $longValue
     *
     * @return SurveyItem
     */
    public function setLongValue($longValue)
    {
        $this->longValue = $longValue;

        return $this;
    }

    /**
     * Get longValue
     *
     * @return string
     */
    public function getLongValue()
    {
        return $this->longValue;
    }
}
