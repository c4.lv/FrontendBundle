<?php

namespace C4\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Locale;

/**
 * Survey
 *
 * @ORM\Table(name="surveys")
 * @ORM\Entity(repositoryClass="C4\FrontendBundle\Repository\SurveyRepository")
 */
class Survey
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="project_id", type="integer")
     */
    private $projectId;

    /**
     * @var int
     *
     * @ORM\Column(name="client_id", type="integer")
     */
    private $clientId;

    /**
     * @var string
     *
     * @ORM\Column(name="title_lv", type="string", length=255, nullable=true)
     */
    private $titleLv;

    /**
     * @var string
     *
     * @ORM\Column(name="title_ru", type="string", length=255, nullable=true)
     */
    private $titleRu;

    /**
     * @var string
     *
     * @ORM\Column(name="description_lv", type="string", length=255, nullable=true)
     */
    private $descriptionLv;

    /**
     * @var string
     *
     * @ORM\Column(name="description_ru", type="string", length=255, nullable=true)
     */
    private $descriptionRu;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=100)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    public function getTitle(){
        return $this->{"getTitle".ucfirst(app()->getLocale())}();
    }
    public function setTitle($title){

        return $this->{"setTitle".ucfirst(app()->getLocale())}($title);
    }
    public function getDescription(){
        return $this->{"getDescription".ucfirst(app()->getLocale())}();
    }
    public function setDescription($description){
        return $this->{"setDescription".ucfirst(app()->getLocale())}($description);
    }

    /**
     * Set titleLv
     *
     * @param string $titleLv
     *
     * @return Survey
     */
    public function setTitleLv($titleLv)
    {
        $this->titleLv = $titleLv;

        return $this;
    }

    /**
     * Get titleLv
     *
     * @return string
     */
    public function getTitleLv()
    {
        return $this->titleLv;
    }

    /**
     * Set titleRu
     *
     * @param string $titleRu
     *
     * @return Survey
     */
    public function setTitleRu($titleRu)
    {
        $this->titleRu = $titleRu;

        return $this;
    }

    /**
     * Get titleRu
     *
     * @return string
     */
    public function getTitleRu()
    {
        return $this->titleRu;
    }

    /**
     * Set descriptionLv
     *
     * @param string $descriptionLv
     *
     * @return Survey
     */
    public function setDescriptionLv($descriptionLv)
    {
        $this->descriptionLv = $descriptionLv;

        return $this;
    }

    /**
     * Get descriptionLv
     *
     * @return string
     */
    public function getDescriptionLv()
    {
        return $this->descriptionLv;
    }

    /**
     * Set descriptionRu
     *
     * @param string $descriptionRu
     *
     * @return Survey
     */
    public function setDescriptionRu($descriptionRu)
    {
        $this->descriptionRu = $descriptionRu;

        return $this;
    }

    /**
     * Get descriptionRu
     *
     * @return string
     */
    public function getDescriptionRu()
    {
        return $this->descriptionRu;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Survey
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set clientId
     *
     * @param integer $clientId
     *
     * @return Survey
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return integer
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     *
     * @return Survey
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return integer
     */
    public function getProjectId()
    {
        return $this->projectId;
    }
}
