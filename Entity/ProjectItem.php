<?php

namespace C4\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Locale;

/**
 * ProjectItem
 *
 * @ORM\Table(name="project_items")
 * @ORM\Entity(repositoryClass="C4\FrontendBundle\Repository\ProjectItemRepository")
 */
class ProjectItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer")
     */
    private $parentId;

    /**
     * @var int
     *
     * @ORM\Column(name="npk", type="integer")
     */
    private $npk;

    /**
     * @var int
     *
     * @ORM\Column(name="project_id", type="integer")
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="title_lv", type="string", length=255, nullable=true)
     */
    private $titleLv;

    /**
     * @var string
     *
     * @ORM\Column(name="title_ru", type="string", length=255, nullable=true)
     */
    private $titleRu;

    /**
     * @var string
     *
     * @ORM\Column(name="description_lv", type="string", length=255, nullable=true)
     */
    private $descriptionLv;

    /**
     * @var string
     *
     * @ORM\Column(name="description_ru", type="string", length=255, nullable=true)
     */
    private $descriptionRu;
    /**
     * @var string
     *
     * @ORM\Column(name="answer_type", type="string", length=100, nullable=true)
     */
    private $answerType;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    public function getTitle(){
        return $this->{"getTitle".ucfirst(app()->getLocale())}();
    }
    public function setTitle($title){

        return $this->{"setTitle".ucfirst(app()->getLocale())}($title);
    }
    public function getDescription(){
        return $this->{"getDescription".ucfirst(app()->getLocale())}();
    }
    public function setDescription($description){
        return $this->{"setDescription".ucfirst(app()->getLocale())}($description);
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return ProjectItem
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set npk
     *
     * @param integer $npk
     *
     * @return ProjectItem
     */
    public function setNpk($npk)
    {
        $this->npk = $npk;

        return $this;
    }

    /**
     * Get npk
     *
     * @return integer
     */
    public function getNpk()
    {
        return $this->npk;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     *
     * @return ProjectItem
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return integer
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set titleLv
     *
     * @param string $titleLv
     *
     * @return ProjectItem
     */
    public function setTitleLv($titleLv)
    {
        $this->titleLv = $titleLv;

        return $this;
    }

    /**
     * Get titleLv
     *
     * @return string
     */
    public function getTitleLv()
    {
        return $this->titleLv;
    }

    /**
     * Set titleRu
     *
     * @param string $titleRu
     *
     * @return ProjectItem
     */
    public function setTitleRu($titleRu)
    {
        $this->titleRu = $titleRu;

        return $this;
    }

    /**
     * Get titleRu
     *
     * @return string
     */
    public function getTitleRu()
    {
        return $this->titleRu;
    }

    /**
     * Set descriptionLv
     *
     * @param string $descriptionLv
     *
     * @return ProjectItem
     */
    public function setDescriptionLv($descriptionLv)
    {
        $this->descriptionLv = $descriptionLv;

        return $this;
    }

    /**
     * Get descriptionLv
     *
     * @return string
     */
    public function getDescriptionLv()
    {
        return $this->descriptionLv;
    }

    /**
     * Set descriptionRu
     *
     * @param string $descriptionRu
     *
     * @return ProjectItem
     */
    public function setDescriptionRu($descriptionRu)
    {
        $this->descriptionRu = $descriptionRu;

        return $this;
    }

    /**
     * Get descriptionRu
     *
     * @return string
     */
    public function getDescriptionRu()
    {
        return $this->descriptionRu;
    }

    /**
     * Set answerType
     *
     * @param string $answerType
     *
     * @return ProjectItem
     */
    public function setAnswerType($answerType)
    {
        $this->answerType = $answerType;

        return $this;
    }

    /**
     * Get answerType
     *
     * @return string
     */
    public function getAnswerType()
    {
        return $this->answerType;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ProjectItem
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
