<?php namespace C4\FrontendBundle\Services;

use Symfony\Component\Yaml\Yaml;

/**
 * Class FrontendSettings
 * @package C4\FrontendBundle\Services
 */
class FrontendSettings
{
    public $settings = null;

    /**
     * FrontendSettings constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        // load yml
        $this->settings = yaml::parse(file_get_contents(__DIR__."/../Resources/config/settings.yml"));
    }
}