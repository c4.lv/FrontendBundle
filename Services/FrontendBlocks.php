<?php namespace C4\FrontendBundle\Services;

use Symfony\Component\Yaml\Yaml;

/**
 * Class FrontendBlocks
 * @package C4\FrontendBundle\Services
 */
class FrontendBlocks
{
    public $blocks = [];

    /**
     * FrontendBlocks constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->blocks = yaml::parse(file_get_contents(__DIR__."/../Resources/config/blocks.yml"));
    }
}