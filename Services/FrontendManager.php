<?php namespace C4\FrontendBundle\Services;

use C4\FrontendBundle\Blocks\BaseBlock;
use C4\FrontendBundle\Services\FrontendBlocks;
use C4\FrontendBundle\Models\User;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Yaml\Yaml;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;

/**
 * Class FrontendManager
 * @package C4\FrontendBundle\Services
 */
class FrontendManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Session
     */
    public $session;

    /**
     * @var \C4\ApiBundle\Services\Api|mixed|object
     */
    public $api;

    /**
     * @var null
     */
    private $settings = null;

    /**
     * @var null
     */
    private $blocks = null;

    /**
     * @var null
     */
    private $validation = null;

    /**
     * @var null
     */
    private $user = null;

    /**
     * FrontendManager constructor.
     * @param Container $container
     */
    function __construct(Container $container)
    {
        $this->container = $container;
        $this->session = new Session();
        $this->api = $this->container->get("api");
    }

    /**
     * @param null $data
     * @param bool $reset
     * @return FrontendSettings|mixed|null
     */
    public function settings($data = null, $reset = false)
    {
        if ($reset) {
            $this->settings = new FrontendSettings();
        }

        // try to load settings from session
        if (!$this->settings) {
            // get settings from session
            $this->settings = $this->session->get("settings");
            // or initialize new settings object
            if (!$this->settings) {
                $this->settings = new FrontendSettings();
            }
        }

        // override old settings with new data
        if ($data) {
            foreach ($data as $key => $val) {
                if (property_exists($this->settings, $key)) {
                    if (is_array($this->settings->$key) && is_array($val)) {
                        $this->settings->$key = array_merge($this->settings->$key, $val);
                    } else {
                        $this->settings->$key = $val;
                    }
                }
            }
        }

        // save and return
        $this->session->set("settings", $this->settings);

        return $this->settings;
    }

    /**
     * @param null $data
     * @param bool $reset
     * @return array|mixed|null
     */
    public function blocks($data = null, $reset = false)
    {
        // todo : force reload
        $this->blocks = (new FrontendBlocks())->blocks;

//        dump( $this->blocks); die();
        return $this->blocks;

        if ($reset) {
            $this->blocks = (new FrontendBlocks())->blocks;
        }

        // try to load blocks from session
        if (!$this->blocks) {
            // get blocks from session
            $this->blocks = $this->session->get("blocks");
            // or initialize new blocks object
            if (!$this->blocks) {
                $this->blocks = (new FrontendBlocks())->blocks;
            }
        }

        // override old blocks with new data
        if ($data) {
            foreach ($data as $key => $val) {
                if (property_exists($this->blocks, $key)) {
                    if (is_array($this->blocks->$key) && is_array($val)) {
                        $this->blocks->$key = array_merge($this->blocks->$key, $val);
                    } else {
                        $this->blocks->$key = $val;
                    }
                }
            }
        }

        // save and return
        $this->session->set("blocks", $this->blocks);

        return $this->blocks;
    }

    /**
     * @return array|null
     */
    public function validation()
    {
        if (!$this->validation) {
            $validation = Yaml::parse(file_get_contents(__DIR__."/../Resources/config/validation.yml"));

            foreach ($validation as $i => $entry) {
                foreach ($entry["fields"] as $field) {
                    foreach ($entry["rules"] as $rule_key => $rule) {
                        if (!isset($this->validation[$field]) || !in_array($rule, $this->validation[$field])) {
                            $this->validation[$field][$rule_key] = $rule;
                        }
                    }
                }
            }
        }

        return $this->validation;
    }

    /**
     * @param null $data
     * @param null $group
     * @return User|mixed|null
     */
    public function user($data = null, $role = null)
    {
        if ($role) {
            $this->user = null;
            $this->session->remove("user");
        } else {
            $role = User::ROLE_GUEST;
        }

        // init user for first time
        if (!$this->user) {
            // get user from session
            $this->user = $this->session->get("user");
            // or initialize new user object
            if (!$this->user) {
                $this->user = new User();
                $this->user->role = $role;
            }
        }

        // override old user data with new data
        if ($data) {


            if (is_object($data)) {
                $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
                $normalizer = new ObjectNormalizer($classMetadataFactory);
                $serializer = new Serializer([$normalizer]);
                $data = $serializer->normalize($data);
            }
            foreach ($data as $key => $val) {
                if (property_exists($this->user, $key)) {
                    if (is_array($this->user->$key) && is_array($val)) {
                        $this->user->$key = array_merge($this->user->$key, $val);
                    } else {
                        $this->user->$key = $val;
                    }
                }
            }
        }

        // save and return
        $this->session->set("user", $this->user);

        return $this->user;
    }

}
