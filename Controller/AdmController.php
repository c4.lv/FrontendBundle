<?php

namespace C4\FrontendBundle\Controller;

use C4\FrontendBundle\Controller\AdmController\addProjectAction;
use C4\FrontendBundle\Controller\AdmController\addProjectAnswerAction;
use C4\FrontendBundle\Controller\AdmController\addProjectQuestionAction;
use C4\FrontendBundle\Controller\AdmController\saveProjectAction;
use C4\FrontendBundle\Controller\AdmController\saveProjectAnswerAction;
use C4\FrontendBundle\Controller\AdmController\saveProjectQuestionAction;
use C4\FrontendBundle\Controller\AdmController\admDashboardAction;
use C4\FrontendBundle\Controller\AdmController\listProjectsAction;
use C4\FrontendBundle\Controller\AdmController\listProjectItemsAction;
use C4\FrontendBundle\Controller\AdmController\sortProjectItemsAction;
use C4\FrontendBundle\Controller\FrontendController\buildHeader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class AdmController extends Controller
{
    public $container;
    /**
     * @var \C4\FrontendBundle\Services\FrontendManager
     */
    public $fm;
    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    public $session;
    /**
     * @var \C4\TranslationBundle\Components\Translator
     */
    public $translator;
    public $request;

    public $router;
    public $route;
    public $route_params;

    public $locale;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        // translator
        $this->translator = $container->get('translator');

        // frontend manager
        $this->fm = $container->get("frontend_manager");

        // current request
        $this->request = $this->container->get('request_stack')->getCurrentRequest();

        // current locale
        $this->locale = $this->request->getLocale();

        // current route
        $this->router = $this->container->get('router');
        $this->route = $this->request->get('_route');
        $this->route_params = $this->request->get('_route_params');

        // session
        $this->session = new Session();

        // route access checking
        if (isset($this->route_params["access"]) && !in_array($this->fm->user()->role, $this->route_params["access"])) {
            throw new  \Exception($this->generateUrl("frontend_index"), 900);
        }

        // save current route in session
        $this->session->set("last_route", $this->route);
        $this->session->set("last_route_params", $this->route_params);
    }

    use buildHeader;

    use admDashboardAction;

    use listProjectsAction;
    use addProjectAction;
    use saveProjectAction;
    // todo : delete

    use listProjectItemsAction;
    use sortProjectItemsAction;

    use addProjectQuestionAction;
    use saveProjectQuestionAction;
    // todo : delete

    use addProjectAnswerAction;
    use saveProjectAnswerAction;
    // todo : delete

}
