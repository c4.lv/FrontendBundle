<?php namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Models\User;

/**
 * Trait buildProjectClientsList
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait buildProjectClientsList
{

    /**
     * @param $base_block
     * @param $project_id
     * @return mixed
     */
    function buildProjectClientsList(&$base_block, $project_id)
    {

        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $em = $this->getDoctrine()->getManager();

        // todo : take clients from ProjectClients (clients added to this project)
        // todo : filter where('questinnaire_id', ..)

        $r = $em->getRepository('FrontendBundle:Client');
        $query = $r->createQueryBuilder('s')
            ->getQuery();
        $clients_list = $query->getResult();

        $clients_list_block = $base_block->withBlock(["class" => "list"]);
        foreach ($clients_list as $client) {
            $clients_list_block->addBlock(
                [
                    "class" => "list-item ",
                    "tag"   => "a",
                    "attr"  => ['href="'.$this->generateUrl("create_survey", ["project_id" => $project_id, "client_id" => $client->getId()]).'"'],
                    "text"  => $client->getFirstName()." ".$client->getLastName()." ".$client->getPhone(),
                ]
            );
        }

        return $clients_list_block;
    }
}
