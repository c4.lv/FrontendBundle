<?php namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Blocks\BaseBlock;
use C4\FrontendBundle\Models\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Trait loginAction
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait loginAction
{

    /**
     * @return RedirectResponse
     */
    function loginAction()
    {
        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $data = $this->request->request->all();
        $locale = $this->request->getLocale();
        $referer = $this->request->headers->get('referer');

        $em = $this->getDoctrine()->getManager();
        $r = $em->getRepository('FrontendBundle:User');
        $user = $r->findOneBy(["email" => $data["email"]]);

        if (!$user) {
            $this->addFlash('error', $this->translator->trans("flash_error_user_not_found", ["default" => "Šāds lietotājs nav atrasts"], "frontend"));

            return new RedirectResponse($referer);
        }

        $encoderService = $this->container->get('security.password_encoder');
        if (!$encoderService->isPasswordValid($user, $data["password"])) {
            $this->addFlash('error', $this->translator->trans("flash_error_wrong_password", ["default" => "Npareiza parole"], "frontend"));

            return new RedirectResponse($referer);
        }

        $this->fm->user($user, $user->getRole());

        return new RedirectResponse($referer);
    }
}
