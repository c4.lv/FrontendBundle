<?php namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Models\User;

/**
 * Trait buildImportantMessages
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait buildImportantMessages
{

    /**
     * @param $base_block
     * @return mixed
     */
    function buildImportantMessages(&$base_block)
    {

        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $base_block->addBlock("text", ["id" => "panel_heading", "text" => $this->translator->trans("important_messages_panel_title", ["default" => '<span class="glyphicon glyphicon-alert"></span> Uzmanību!'], "frontend"), "standalone" => "true"]);

        // todo : demo message - implementations required
        $base_block->addBlock(
            [
                "class" => "with-padding",
                "text"  => "Lūdzu pievērst uzmanību 2.-jam jautājumam! <br><br>Citas piezīmes konkrētajai anketai",
            ]
        );

        return $base_block;

    }
}
