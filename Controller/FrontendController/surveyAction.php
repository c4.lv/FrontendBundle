<?php

namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Models\User;

/**
 * Trait surveyAction
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait surveyAction
{

    /**
     * @param $id
     * @param $survey_id
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    function surveyAction($project_id, $survey_id)
    {
        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $page = block("page");

        $this->buildHeader($page);

        $content = $page->withBlock("block", ["id" => "content"]);

        $em = $this->getDoctrine()->getManager();
        $project = $em->find('FrontendBundle:Project', $project_id);

        $left_side = $content->withBlock(["class" => ["col-md-4"]]);

        $left_panel = $left_side->withBlock("panel", ["class" => ["panel-warning clear-panel-body"]]);
        $this->buildProjectList($left_panel, $project_id);


        $left_panel2 = $left_side->withBlock("panel", ["class" => ["panel-warning clear-panel-body"]]);
        $this->buildImportantMessages($left_panel2);

        $right_panel = $content->withBlock("block", ["class" => ["col-md-8"]])->withBlock("panel", ["class" => ["panel-success"]]);
        $right_panel->addBlock(
            "text",
            [
                "id"         => "panel_heading",
                "text"       => $project->getTitle(),
                "standalone" => "true",
            ]
        );

        // todo : continue survey implementation here...

        return $page->render(true);
    }
}
