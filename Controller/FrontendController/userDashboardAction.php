<?php

namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Models\User;

/**
 * Trait userDashboardAction
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait userDashboardAction
{

    /**
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    function userDashboardAction()
    {
        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $page = block("page");

        $this->buildHeader($page);

        $content = $page->withBlock("block", ["id" => "content"]);

        $left_panel = $content->withBlock(["class" => ["col-md-4"]])->withBlock("panel", ["class" => ["panel-warning clear-panel-body"]]);
        $this->buildProjectList($left_panel);

        $right_panel = $content->withBlock("block", ["class" => ["col-md-8"]])->withBlock("panel", ["class" => ["panel-success"]]);
        $right_panel->addBlock(
            "text",
            [
                "id"         => "panel_heading",
                "text"       => "Labdien, ".(($this->fm->user()->first_name.$this->fm->user()->last_name != "")
                    ? $this->fm->user()->first_name." ".$this->fm->user()->last_name
                    : $this->fm->user()->email)
                ,
                "standalone" => "true",
            ]
        );

        // todo : continue dashboard implementation
        $right_panel->addBlock(
            [
                "text" => "Lai uzsāktu darbu, izvēlies anketu. <br><br>TODO: <br> - zvanītāja statistika <br> - pēdējais zvans  <br> - nepabeigto anketu saraksts  <br> - ziņojumi no administrācijas <br> - etc",
            ]
        );

        return $page->render(true);
    }
}
