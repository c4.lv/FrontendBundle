<?php

namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Blocks\BaseBlock;
use C4\FrontendBundle\Models\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Trait logoutAction
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait logoutAction
{
    /**
     * @return RedirectResponse
     */
    function logoutAction()
    {
        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $data = $this->request->request->all();
        $locale = $this->request->getLocale();
        $referer = $this->request->headers->get('referer');

        $this->session->clear();

        return new RedirectResponse($referer);
    }
}
