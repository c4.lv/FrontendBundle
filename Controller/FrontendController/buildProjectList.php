<?php namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Models\User;

/**
 * Trait buildProjectList
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait buildProjectList
{

    /**
     * @param $base_block
     * @param null $selected_id
     * @return mixed
     */
    function buildProjectList(&$base_block, $selected_id = null)
    {

        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $em = $this->getDoctrine()->getManager();
        $r = $em->getRepository('FrontendBundle:Project');
        $query = $r->createQueryBuilder('s')
            ->getQuery();
        $project_list = $query->getResult();

        $base_block->addBlock("text", ["id" => "panel_heading", "text" => $this->translator->trans("active_surveys_title", ["default" => "Aktīvās anketas"], "frontend"), "standalone" => "true"]);

        $project_list_block = $base_block->withBlock(["class" => "list"]);
        foreach ($project_list as $project_item) {
            $project_list_block->addBlock(["class" => "list-item ".(($project_item->getId() == $selected_id) ? "selected" : ""), "tag" => "a", "attr" => ['href="'.$this->generateUrl("user_project", ["id" => $project_item->getId()]).'"'], "text" => $project_item->getTitle()]);
        }

        return $project_list_block;
    }
}
