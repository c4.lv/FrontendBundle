<?php

namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Models\User;

/**
 * Trait projectAction
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait projectAction
{
    /**
     * @param $id
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    function projectAction($id)
    {
        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $page = block("page");

        $this->buildHeader($page);

        $content = $page->withBlock("block", ["id" => "content"]);

        $em = $this->getDoctrine()->getManager();
        $project = $em->find('FrontendBundle:Project', $id);

        $left_side = $content->withBlock(["class" => ["col-md-4"]]);

        $left_panel = $left_side->withBlock("panel", ["class" => ["panel-warning clear-panel-body"]]);
        $this->buildProjectList($left_panel, $id);


        $left_panel2 = $left_side->withBlock("panel", ["class" => ["panel-warning clear-panel-body"]]);
        $this->buildImportantMessages($left_panel2);


        $right_panel = $content->withBlock("block", ["class" => ["col-md-8"]])->withBlock("panel", ["class" => ["panel-success"]]);
        $right_panel->addBlock(
            "text",
            [
                "id"         => "panel_heading",
                "text"       => $project->getTitle(),
                "standalone" => "true",
            ]
        );

        $subpanel_1 = $right_panel->withBlock("panel", ["class" => ["panel-default panel-inner"]]);
        $subpanel_1->addBlock(
            "text",
            [
                "id"         => "panel_heading",
                "text"       => $this->translator->trans("started_surveys_title", ["default" => "Nepabeigtās anketas"], "frontend"),
                "standalone" => "true",
            ]
        );
        $this->buildSurveyList($subpanel_1, $id);

        // todo : create surveys from available resources
        $subpanel_2 = $right_panel->withBlock("panel", ["class" => ["panel-default panel-inner"]]);
        $subpanel_2->addBlock(
            "text",
            [
                "id"         => "panel_heading",
                "text"       => "Izveidot jaunu anketu",
                "standalone" => "true",
            ]
        );
        $this->buildProjectClientsList($subpanel_2, $id);

        return $page->render(true);
    }
}
