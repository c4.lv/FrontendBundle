<?php namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Models\User;

/**
 * Trait buildSurveyList
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait buildSurveyList
{
    /**
     * @param $base_block
     * @param null $selected_id
     * @return mixed
     */
    function buildSurveyList(&$base_block, $selected_id = null)
    {

        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $em = $this->getDoctrine()->getManager();
        $r = $em->getRepository('FrontendBundle:Survey');
        $query = $r->createQueryBuilder('s')
            ->where("s.projectId = :project_id")->setParameter("project_id", $selected_id)
            ->getQuery();
        $survey_list = $query->getResult();

        $project_list_block = $base_block->withBlock(["class" => "list"]);
        foreach ($survey_list as $survey_item) {

            $client = $em->getRepository('FrontendBundle:Client')->find($survey_item->getClientId());

            $project_list_block->addBlock(
                [
                    "class" => "list-item ".(($survey_item->getId() == $selected_id) ? "selected" : ""),
                    "tag"   => "a",
                    "attr"  => ['href="'.$this->generateUrl("user_survey", ["project_id" => $selected_id, "survey_id" => $survey_item->getId()]).'"'],
                    "text"  => $client->getFirstName()." ".$client->getLastName()." ".$client->getPhone(),
                ]
            );
        }

        return $project_list_block;
    }
}
