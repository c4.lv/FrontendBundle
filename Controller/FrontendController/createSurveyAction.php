<?php

namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Blocks\BaseBlock;
use C4\FrontendBundle\Entity\Survey;
use C4\FrontendBundle\Entity\SurveyItem;
use C4\FrontendBundle\Models\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Trait createSurveyAction
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait createSurveyAction
{

    /**
     * @param $project_id
     * @param $client_id
     * @return RedirectResponse
     */
    function createSurveyAction($project_id, $client_id)
    {
        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $data = $this->request->request->all();
        $locale = $this->request->getLocale();
        $referer = $this->request->headers->get('referer');

        $em = $this->getDoctrine()->getManager();

        $client = $em->find('FrontendBundle:Client', $client_id);
        $project = $em->find('FrontendBundle:Project', $project_id);

        $survey = $em->getRepository('FrontendBundle:Survey')->findOneBy(["projectId" => $project_id, "clientId" => $client_id]);
        if ($survey) {
            // todo : handle "project already exists"
            die("todo : handle - client project already exists");
        }

        $r = $em->getRepository('FrontendBundle:ProjectItem');
        $query = $r->createQueryBuilder('s')
            ->addOrderBy('s.parentId', 'ASC')
            ->addOrderBy('s.npk', 'ASC')
            ->where('s.projectId = :project_id')->setParameter('project_id', $project_id)
            ->getQuery();
        $project_items_list = $query->getResult();
        $project_items_list_array = entity_to_array($project_items_list);

        $survey = new Survey();

        $survey->setProjectId($project->getId());

        $survey->setTitleLv($project->getTitleLv());
        $survey->setTitleRu($project->getTitleRu());
        $survey->setDescriptionLv($project->getDescriptionLv());
        $survey->setDescriptionRu($project->getDescriptionRu());

        $survey->setClientId($client->getId());
        $survey->setStatus("new"); // new pending done paused

        $em->persist($survey);
        $em->flush();

        // save id`s of cloned items, so we can find parent id`s for child elements
        // $created_items["old_id"] = new_id
        $created_items = [];
        foreach ($project_items_list as $project_item) {
            $survey_item = new SurveyItem();

            $parent_id = 0;
            if ($project_item->getParentId() > 0) {
                $parent = $project_items_list_array[array_search($project_item->getParentId(), array_column($project_items_list_array, 'id'))];
                $parent_id = $parent["id"];
            }
            $survey_item->setParentId($parent_id);

            $survey_item->setSurveyId($survey->getId());

            $survey_item->setTitleLv($project_item->getTitleLv());
            $survey_item->setTitleRu($project_item->getTitleRu());
            $survey_item->setDescriptionLv($project_item->getDescriptionLv());
            $survey_item->setDescriptionRu($project_item->getDescriptionRu());
            $survey_item->setNpk($project_item->getNpk());
            $survey_item->setType($project_item->getType());
            $survey_item->setAnswerType($project_item->getAnswerType());

            $em->persist($survey_item);
            $em->flush();
            $created_items[$project_item->getId()] = $survey_item->getId();
        }

        return new RedirectResponse($referer);
    }
}
