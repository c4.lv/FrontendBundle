<?php namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Models\User;

/**
 * Trait buildHeader
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait buildHeader
{

    /**
     * @param $base_block
     * @return mixed
     */
    function buildHeader(&$base_block)
    {

        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $header = $base_block->withBlock("header", ["id" => "header"]);
        $header->addBlock("block", ["tag" => "h5", "attr" => ['style="display:inline;"'], "text" => $this->translator->trans("logo_title", ["default" => '<a href="/">info@lmt.lv</a>'], "frontend"),]);

        // todo : language switch
        //        $header->addBlock("block", ["text" => "LV", "class" => ['pull-right']]);

        if ($this->fm->user()->role != User::ROLE_GUEST) {

            $header->addBlock(
                "block",
                [
                    "tag"   => "a",
                    "attr"  => ['href="'.$this->generateUrl("frontend_logout").'"'],
                    // todo : logout button (at the moment temporary)
                    "text" =>'<span class="glyphicon glyphicon-log-out"></span>',
                    "class" => ['pull-right', 'header-btn'],
                ]
            );

            $header->addBlock(
                "block",
                [
                    "tag"   => "span",
                    "text" =>
                        ($this->fm->user()->first_name.$this->fm->user()->last_name != "")
                            ?'<span class="glyphicon glyphicon-user"></span> '.$this->fm->user()->first_name." ".$this->fm->user()->last_name
                            :'<span class="glyphicon glyphicon-user"></span> '.$this->fm->user()->email
                    ,
                    "class" => ['pull-right', 'header-btn'],
                ]
            );

        }
        // todo : search
//        $header->addBlock("block", ["text" => "Search", "class" => ['pull-right']]);

        return $header;
    }
}
