<?php

namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Blocks\BaseBlock;
use C4\FrontendBundle\Models\User;

/**
 * Trait indexAction
 * @package C4\FrontendBundle\Controller\FrontendController
 */
trait indexAction
{

    /**
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    function indexAction()
    {
        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $page = block("page");
        $this->buildHeader($page);

        $content = $page->withBlock("block", ["id" => "content", "class" => ['login-container']]);

        $panel = $content->withBlock("block", ["class" => ["col-md-6 col-md-offset-3"]])->withBlock("panel", ["class" => ["panel-success"]]);
        $panel->addBlock("text", ["id" => "panel_heading", "text" => "Autorizācija", "standalone" => "true"]);
        $panel->addBlock("login_form", []);

        $content->addBlock(
            "block",
            [
                "attr" => ['style="clear:both;"'],
            ]
        );

        // todo : footer - do we need it in adm panel?
//        $page->addBlock(
//            "footer",
//            [
//                "id"   => "footer",
//                "text" => '<div style="text-align:center;">&copy; LMT  2017</div>',
//            ]
//        );

        return $page->render(true);
    }
}
