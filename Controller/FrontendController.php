<?php

namespace C4\FrontendBundle\Controller;

use C4\FrontendBundle\Blocks\BaseBlock;
use C4\FrontendBundle\Controller\FrontendController\buildImportantMessages;
use C4\FrontendBundle\Controller\FrontendController\buildSurveyList;
use C4\FrontendBundle\Controller\FrontendController\buildProjectClientsList;
use C4\FrontendBundle\Controller\FrontendController\buildHeader;
use C4\FrontendBundle\Controller\FrontendController\buildProjectList;
use C4\FrontendBundle\Controller\FrontendController\createSurveyAction;
use C4\FrontendBundle\Controller\FrontendController\loginAction;
use C4\FrontendBundle\Controller\FrontendController\logoutAction;
use C4\FrontendBundle\Controller\FrontendController\userDashboardAction;
use C4\FrontendBundle\Controller\FrontendController\surveyAction;
use C4\FrontendBundle\Controller\FrontendController\projectAction;
use C4\FrontendBundle\Models\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

use C4\FrontendBundle\Controller\FrontendController\indexAction;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FrontendController
 * @package C4\FrontendBundle\Controller
 */
class FrontendController extends Controller
{
    public $container;
    /**
     * @var \C4\FrontendBundle\Services\FrontendManager
     */
    public $fm;
    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    public $session;
    /**
     * @var \C4\TranslationBundle\Components\Translator
     */
    public $translator;
    public $request;

    public $router;
    public $route;
    public $route_params;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        // translator
        $this->translator = $container->get('translator');

        // frontend manager
        $this->fm = $container->get("frontend_manager");

        // current request
        $this->request = $this->container->get('request_stack')->getCurrentRequest();

        // current route
        $this->router = $this->container->get('router');
        $this->route = $this->request->get('_route');
        $this->route_params = $this->request->get('_route_params');

        // session
        $this->session = new Session();

        // route access checking
        if (isset($this->route_params["access"]) && !in_array($this->fm->user()->role, $this->route_params["access"])) {

            if ($this->fm->user()->role == User::ROLE_USER){
                throw new  \Exception($this->generateUrl("user_dashboard"), 900);
            }

            if ($this->fm->user()->role == User::ROLE_ADMIN){
                throw new  \Exception($this->generateUrl("adm_dashboard"), 900);
            }

            throw new  \Exception($this->generateUrl("frontend_index"), 900);
        }

        // save current route in session
        $this->session->set("last_route", $this->route);
        $this->session->set("last_route_params", $this->route_params);
    }

    use buildHeader;

    use indexAction;
    use loginAction;
    use logoutAction;

    use userDashboardAction;
    use projectAction;
    use surveyAction;

    use buildProjectList;
    use buildProjectClientsList;
    use buildSurveyList;

    use buildImportantMessages;

    use createSurveyAction;

}