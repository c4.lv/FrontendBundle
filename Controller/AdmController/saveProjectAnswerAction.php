<?php

namespace C4\FrontendBundle\Controller\AdmController;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Trait saveProjectAnswerAction
 * @package C4\FrontendBundle\Controller\AdmController
 */
trait saveProjectAnswerAction
{
    /**
     * @param $id
     * @param $answer_id
     * @return RedirectResponse
     */
    function saveProjectAnswerAction($id, $answer_id)
    {
        /** @var \C4\FrontendBundle\Controller\AdmController $this */

        $data = $this->request->request->all();

        $em = $this->getDoctrine()->getManager();

        $project_item = $em->find('FrontendBundle:ProjectItem', $answer_id);

        $project_item->setTitle($data["title"]);

        if (isset($data["description"])) {
            $project_item->setDescription($data["description"]);
        }

        $em->persist($project_item);
        $em->flush();

        $referer = $this->request->headers->get('referer');

//        $this->request->getSession()->setFlash('success', "Successfully saved");
        return new RedirectResponse($referer);
    }
}
