<?php

namespace C4\FrontendBundle\Controller\AdmController;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Trait saveProjectAction
 * @package C4\FrontendBundle\Controller\AdmController
 */
trait saveProjectAction
{

    /**
     * @param $id
     * @return RedirectResponse
     */
    function saveProjectAction($id)
    {
        /** @var \C4\FrontendBundle\Controller\AdmController $this */

        $data = $this->request->request->all();

        $em = $this->getDoctrine()->getManager();

        $project = $em->find('FrontendBundle:Project', $id);

        $project->setTitle($data["title"]);
        $project->setDescription($data["description"]);

        $em->persist($project);
        $em->flush();

        $referer = $this->request->headers->get('referer');

//        $this->request->getSession()->setFlash('success', "Successfully saved");
        return new RedirectResponse($referer);
    }
}
