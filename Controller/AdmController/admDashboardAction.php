<?php

namespace C4\FrontendBundle\Controller\AdmController;

use C4\FrontendBundle\Blocks\BaseBlock;

/**
 * Trait admDashboardAction
 * @package C4\FrontendBundle\Controller\AdmController
 */
trait admDashboardAction
{

    /**
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    function adminDashboardAction()
    {
        /** @var \C4\FrontendBundle\Controller\AdmController $this */

        $page = block("page");
        $header = $this->buildHeader($page);
        $content = $page->withBlock("block", ["id" => "content"]);

        $left_panel = $content->withBlock(["class" => ["col-md-4"]])->withBlock("panel", ["class" => ["panel-warning clear-panel-body"]]);
        $left_panel->addBlock("text", ["id" => "panel_heading", "text" => $this->translator->trans("adm_dashboard_menu_title", ["default" => "Rīki"], "frontend"), "standalone" => "true"]);

        $left_panel
            ->withBlock(["class" => ['list']])
            ->addBlock(
                [
                    "tag"   => "a",
                    "attr"  => ['href="'.$this->generateUrl("adm_projects").'" target="_blank"'],
                    "class" => ['list-item'],
                    "text"  => $this->translator->trans("project_list_title", ["default" => "Projekti"], "frontend"),
                ]
            );

        $right_panel = $content->withBlock("block", ["class" => ["col-md-8"]])->withBlock("panel", ["class" => ["panel-success"]]);
        $right_panel->addBlock(
            "text",
            [
                "id"         => "panel_heading",
                "text"       => $this->translator->trans("adm_dashboard_title", ["default" => "Dashboard"], "frontend"),
                "standalone" => "true",
            ]
        );

        return $page->render(true);
    }
}
