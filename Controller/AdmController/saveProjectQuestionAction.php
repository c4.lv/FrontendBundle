<?php

namespace C4\FrontendBundle\Controller\AdmController;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Trait saveProjectQuestionAction
 * @package C4\FrontendBundle\Controller\AdmController
 */
trait saveProjectQuestionAction
{

    /**
     * @param $id
     * @param $question_id
     * @return RedirectResponse
     */
    function saveProjectQuestionAction($id, $question_id)
    {
        /** @var \C4\FrontendBundle\Controller\AdmController $this */

        $data = $this->request->request->all();

        $em = $this->getDoctrine()->getManager();

        $project_item = $em->find('FrontendBundle:ProjectItem', $question_id);

        $project_item->setTitle($data["title"]);

        if (isset($data["description"])) {
            $project_item->setDescription($data["description"]);
        }
        if (isset($data["answer_type"])) {
            $project_item->setAnswerType($data["answer_type"]);
        }

        $em->persist($project_item);
        $em->flush();

        $referer = $this->request->headers->get('referer');

//        $this->request->getSession()->setFlash('success', "Successfully saved");
        return new RedirectResponse($referer);
    }
}
