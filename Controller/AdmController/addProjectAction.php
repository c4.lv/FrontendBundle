<?php

namespace C4\FrontendBundle\Controller\AdmController;

use C4\FrontendBundle\Blocks\BaseBlock;

use C4\FrontendBundle\Entity\Project;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Trait addProjectAction
 * @package C4\FrontendBundle\Controller\AdmController
 */
trait addProjectAction
{

    /**
     * @return RedirectResponse
     */
    function addProjectAction()
    {
        /** @var \C4\FrontendBundle\Controller\AdmController $this */

        $data = $this->request->request->all();
        $locale = $this->request->getLocale();

        $em = $this->getDoctrine()->getManager();

        $project = new Project();
        $project->setTitle($this->translator->trans("new_project_default_title", ["default" => "Jauns projekts"], "frontend"));

        $em->persist($project);
        $em->flush();

        return new RedirectResponse($this->generateUrl("adm_project", ["id" => $project->getId()]));
    }
}
