<?php

namespace C4\FrontendBundle\Controller\AdmController;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait sortProjectItemsAction
 * @package C4\FrontendBundle\Controller\AdmController
 */
trait sortProjectItemsAction
{

    /**
     * @param $id
     */
    function sortProjectItemsAction($id)
    {
        /** @var \C4\FrontendBundle\Controller\AdmController $this */

        $data = $this->request->request->get("data");

        $data = json_decode($data, true);

        $parsed = [];
        $this->parse($parsed, $data);

        $em = $this->getDoctrine()->getManager();

        foreach($parsed as $item){
            $object = $em->find('FrontendBundle:ProjectItem', $item["id"]);
            $object->setParentId($item["parent_id"]);
            $object->setNpk($item["npk"]);
            $em->persist($object);
            $em->flush();
        }
        $r = new Response('Content', Response::HTTP_OK);
//        $r->headers->set('Content-Type', 'application/json');
        $r->headers->set('Content-Type', 'text/html');
        $r->setContent(
            json_encode(
                [

                ]
            )
        );
        $r->send();
    }

    function parse(&$base, $childrens, $parent_id = 0){
        $npk = 0;
        foreach($childrens as $child){
            $child["parent_id"] = $parent_id;
            if ($child["id"] != 0){
                $npk++;
                $child["npk"] = $npk;
                $base[] = $child;
            }
            if (isset($child["children"]) && count($child["children"]) > 0){
                $this->parse($base, $child["children"], $child["id"]);
            }
        }
    }
}
