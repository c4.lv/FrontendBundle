<?php

namespace C4\FrontendBundle\Controller\AdmController;

use C4\FrontendBundle\Blocks\BaseBlock;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait listProjectItemsAction
 * @package C4\FrontendBundle\Controller\AdmController
 */
trait listProjectItemsAction
{

    /**
     * @param null $id
     * @param null $item_id
     * @return string|Response
     */
    function listProjectItemsAction($id = null, $item_id = null)
    {
        /** @var \C4\FrontendBundle\Controller\AdmController $this */

        $page = block("page");
        $header = $this->buildHeader($page);
        $content = $page->withBlock("block", ["id" => "content"]);

        $em = $this->getDoctrine()->getManager();
        $project = $em->find('FrontendBundle:Project', $id);

        $r = $em->getRepository('FrontendBundle:ProjectItem');
        $query = $r->createQueryBuilder('s')
            ->addOrderBy('s.parentId', 'ASC')
            ->addOrderBy('s.npk', 'ASC')
            ->where('s.projectId = :id')->setParameter('id', $id)
            ->getQuery();
        $project_items_list = $query->getResult();

        $main_items = [];
        $child_items = [];
        foreach ($project_items_list as $i => $item) {
            if (!$item->getParentId()) {
                $main_items[] = $item;
            } else {
                $child_items[$item->getParentId()][] = $item;
            }
        }

        $treeview_data = [];
        foreach ($main_items as $i => $item) {

            $children = [];
            if (isset($child_items[$item->getId()])) {
                foreach ($child_items[$item->getId()] as $child_item) {
                    $children[] =
                        [
                            "id"       => $child_item->getId(),
                            "label"    => '<a href="'.$this->generateUrl("adm_project_item", ["id" => $id, "item_id" => $child_item->getId()]).'">'.$child_item->getTitle().'</a>',
                            "children" => [],
                        ];
                }
            }

            $children[] = [
                "id"       => '0',
                "label"    => '<i class="pull-right"><a href="'.$this->generateUrl("adm_project_add_answer", ["id" => $id, "parent_id" => $item->getId()]).'" style="color:green;">'.$this->translator->trans("adm_add_answer_btn", ["default" => " + pievienot atbildi", "frontend"]).'</a></i>',
                "children" => [],
            ];

            $treeview_data[] = [
                "id"       => $item->getId(),
                "label"    => '<a href="'.$this->generateUrl("adm_project_item", ["id" => $id, "item_id" => $item->getId()]).'"><b>'.$item->getTitle().'</b></a>',
                "children" => $children,
            ];
        }

        $left_panel = $content->withBlock(["class" => ["col-md-4"]])->withBlock("panel", ["class" => ["panel-warning clear-panel-body"]]);
        $left_panel->addBlock(
            "text",
            [
                "id"         => "panel_heading",
                "text"       => '<a href="'.$this->generateUrl("adm_project", ["id" => $project->getId()]).'">&#8617; '.$project->getTitle().'</a><a href="'.$this->generateUrl("adm_project_add_question", ["id" => $id]).'" class="pull-right">'.$this->translator->trans(
                        "adm_add_question_btn",
                        ["default" => "+ jauns jautājums"],
                        "frontend"
                    ).'</a>',
                "standalone" => "true",
            ]
        );

        $left_panel
            ->withBlock(["class" => 'with-padding'])
            ->addBlock(
                "adm_treeview",
                [
                    "id"               => "left_menu",
                    "data"             => json_encode($treeview_data),
                    "tree_id"          => $project->getId(),
                    "selected"         => $item_id,
                    "ajax_sorting_url" => $this->generateUrl("adm_sort_project_items", ["id" => $project->getId()]),
                ]
            );

        $right_panel = $content->withBlock("block", ["class" => ["col-md-8"]])->withBlock("panel", ["class" => ["panel-success"]]);

        if ($item_id) {
            $project_item = $em->find('FrontendBundle:ProjectItem', $item_id);

            $right_panel->addBlock("text", ["id" => "panel_heading", "text" => "Uzstādījumi", "standalone" => "true"]);

            $right_panel->withBlock(["class" => "with-padding"])->addBlock(

                'adm_project_'.$project_item->getType().'_form',
                [
                    "project"      => $project,
                    "project_item" => $project_item,
                ]
            );
        } else {
            $right_panel->withBlock("text", ["id" => "panel_body", "text" => ""]);
        }

        return $page->render(true);
    }
}
