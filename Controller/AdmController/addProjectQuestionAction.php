<?php

namespace C4\FrontendBundle\Controller\AdmController;

use C4\FrontendBundle\Blocks\BaseBlock;

use C4\FrontendBundle\Entity\Project;
use C4\FrontendBundle\Entity\ProjectItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Trait addProjectQuestionAction
 * @package C4\FrontendBundle\Controller\AdmController
 */
trait addProjectQuestionAction
{

    /**
     * @param $id
     * @param int $parent_id
     * @return RedirectResponse
     */
    function addProjectQuestionAction($id, $parent_id = 0)
    {
        /** @var \C4\FrontendBundle\Controller\AdmController $this */

        $data = $this->request->request->all();

        $em = $this->getDoctrine()->getManager();

        $r = $em->getRepository('FrontendBundle:ProjectItem');
        $query = $r->createQueryBuilder('s')
            ->where('s.projectId = :id')->setParameter('id', $id)
            ->andWhere('s.parentId = :pid')->setParameter('pid', 0)
            ->getQuery();
        $project_items_list = $query->getResult();
        $count = count($project_items_list);

        $project_item = new ProjectItem();

        $project_item->setProjectId($id);
        $project_item->setParentId($parent_id);
        $project_item->setTitle($this->translator->trans("default_question_title", ["default" => "Jauns jautājums"], "frontend"));
        $project_item->setAnswerType("text");
        $project_item->setType("question");
        $project_item->setNpk($count + 1);

        $em->persist($project_item);
        $em->flush();

        return new RedirectResponse($this->generateUrl("adm_project_item", ["id" => $id, "item_id" => $project_item->getId()]));
    }
}
