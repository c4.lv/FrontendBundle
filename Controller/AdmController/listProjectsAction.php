<?php

namespace C4\FrontendBundle\Controller\AdmController;

use C4\FrontendBundle\Blocks\BaseBlock;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait listProjectsAction
 * @package C4\FrontendBundle\Controller\AdmController
 */
trait listProjectsAction
{

    /**
     * @param null $id
     * @return string|Response
     */
    function listProjectsAction($id = null)
    {
        /** @var \C4\FrontendBundle\Controller\AdmController $this */

        $page = block("page");
        $header = $this->buildHeader($page);
        $content = $page->withBlock("block", ["id" => "content"]);

        $em = $this->getDoctrine()->getManager();

        $left_panel = $content->withBlock(["class" => ["col-md-4"]])->withBlock("panel", ["class" => ["panel-warning clear-panel-body"]]);
        $left_panel->addBlock(
            "text",
            [
                "id"         => "panel_heading",
                "text"       => $this->translator->trans("adm_projects_list_title", ["default" => "Projekti"], "frontend").' <a href="'.$this->generateUrl("adm_add_project").'" class="pull-right">'.$this->translator->trans("adm_add_project_btn", ["default" => "+ jauns projekts"], "frontend").'</a>',
                "standalone" => "true",
            ]
        );

        $r = $em->getRepository('FrontendBundle:Project');
        $query = $r->createQueryBuilder('s')
            ->getQuery();
        $project_list = $query->getResult();
        $project_list_block = $left_panel->withBlock(["class" => "list"]);
        foreach ($project_list as $each_project) {
            $project_list_block->addBlock(
                [
                    "class" => "list-item ".(($each_project->getId() == $id) ? "selected" : ""),
//                    "tag"   => "a",
                    "attr"  => ['href="'.$this->generateUrl("user_project", ["id" => $each_project->getId()]).'"'],
                    "text"  => '<a href="'.$this->generateUrl("adm_project", ["id" => $each_project->getId()]).'">'.$each_project->getTitle().'</a>'.' <a href="'.$this->generateUrl(
                            "adm_project_items",
                            ["id" => $each_project->getId()]
                        ).'" class="pull-right list-item-addon">'.$this->translator->trans("adm_questions_btn", ["default" => "jautājumi"], "frontend").'</a>',
                ]
            );
        }

        $right_panel = $content->withBlock("block", ["class" => ["col-md-8"]])->withBlock("panel", ["class" => ["panel-success"]]);

        if ($id) {
            $project = $em->find('FrontendBundle:Project', $id);

            $right_panel->addBlock("text", ["id" => "panel_heading", "text" => $this->translator->trans("adm_project_settings_title", ["default" => "Projekta uzstādījumi"], "frontend"), "standalone" => "true"]);

            $right_panel->withBlock(["class" => "with-padding"])->addBlock(
                'adm_project_form',
                [
                    "class"   => ["with-padding"],
                    "project" => $project,
                    "action"  => $this->generateUrl("adm_save_project", ["id" => $id]),
                ]
            );
        } else {
            $right_panel->withBlock("text", ["id" => "panel_body", "text" => '']);
        }

        return $page->render(true);
    }
}
