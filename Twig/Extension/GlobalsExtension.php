<?php namespace C4\FrontendBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GlobalsExtension
 * @package C4\FrontendBundle\Twig\Extension
 */
class GlobalsExtension extends \Twig_Extension
{
    /**
     * GlobalsExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getGlobals()
    {
        return [
            'fm'          => $this->container->get('frontend_manager'),
            'application' => app(),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'FrontendBundle:GlobalsExtension';
    }
}