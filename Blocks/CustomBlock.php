<?php namespace C4\FrontendBundle\Blocks;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CustomBlock
 * @package C4\FrontendBundle\Blocks
 * used when we need additional block functionality
 */
class CustomBlock extends BaseBlock
{
    public function __construct(
        ContainerInterface $container,
        $block_name,
        array $settings = [],
        $parent_block = null
    )
    {
        $this->settings = array_merge($this->settings, [
            "data"       => "",
        ]);
        parent::__construct($container, $block_name, $settings, $parent_block);
    }

}