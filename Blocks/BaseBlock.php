<?php namespace C4\FrontendBundle\Blocks;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseBlock
 * @package C4\FrontendBundle\Blocks
 */
class BaseBlock
{
    /**
     * @var
     */
    private $container;

    /**
     * @var null
     */
    public $parent_block;

    /**
     * @var null|string
     */
    public $block_name = "base_block";

    /**
     * @var array
     */
    public $settings = [
        "render"     => true, // if false, return "" from function render() - used in cases when we need block data after rendering
        "standalone" => false, // if true, skip block in foreach blocks

        "tag"         => "div",
        "for"         => "",
        "id"          => "",
        "name"        => "",
        "value"       => "",
        "placeholder" => "",
        "title"       => "",
        "text"        => "",

        "class"      => [],
        "attr"       => [],
        "validation" => [],
        "blocks"     => [],

        "active"   => false,
        "disabled" => false,
        "hidden"   => false,

        "route"            => "",
        "route_parameters" => [],
        "href"             => "",
    ];

    public $standalone = false;
    public $template = "";

    /**
     * BaseBlock constructor.
     * @param null $block_name
     * @param array $settings
     * @param null $parent_block
     */
    public function __construct($block_name = null, $settings = [], $parent_block = null)
    {

        $this->container = app()->getContainer();

        $this->block_name = $block_name;
        $this->parent_block = $parent_block;
        $this->initSettings($settings);
        $this->block_name = $this->settings["block_name"] = $block_name;
    }

    /**
     * @param array $settings
     */
    public function initSettings($settings = [])
    {
        $frontend = $this->container->get("frontend_manager");

        // load settings from yaml
        $this->settings = array_replace(
            $this->settings,
            $frontend->blocks()[$this->block_name]
        );

        // if block is ("form_group_input" | "checkbox" | "radio") and has name - parse validation for block
        if (in_array($this->block_name, ["form_group_input", "checkbox", "radio"]) && isset($settings["name"])) {

            // load validations from yaml
            if (isset($frontend->validation()["all"])) {
                $this->settings["validation"] = $frontend->validation()["all"];
            } else {
                $this->settings["validation"] = [];
            }

            // append validation rules
            if (isset($frontend->validation()[$settings["name"]])) {
                foreach ($frontend->validation()[$settings["name"]] as $i => $value) {
                    if (!in_array($value, $this->settings["validation"])) {
                        $this->settings["validation"][] = $value;
                    }
                }
            }
        }

        // override settings passed in array
        // clear empty values - we dont want to replace defaults with empty values
        foreach ($settings as $key => $val) {
            if ($val == "" && key_exists($key, $this->settings)) {
                unset($settings[$key]);
            }
        }
        // override defined settings with block constructor specified settings (block constructor settings overrides any other settings)
        $this->settings = array_replace($this->settings, $settings);

        // set custom template if specified
        if (isset($this->settings["template"])) {
            $this->template = $this->settings["template"];
        }

        // set block as standalone if specified in settings
        if (isset($this->settings["standalone"])) {
            $this->standalone = $this->settings["standalone"];
        }
    }

    /**
     * @param $block_name
     * @param array $settings
     * @return $this
     */
    public function addBlock($block_name, $settings = [])
    {

        // we can build default block by providing only settings
        if (is_array($block_name)) {
            $settings = $block_name;
            $block_name = "block";
        }

        // we can pass already construed block
        if (!is_string($block_name)) {
            $block = $block_name;
            // add block parental reference
            $block->parent_block = $this;
        }

        // build new block from specified constructor
        else {
            $block_class = 'C4\FrontendBundle\Blocks\\'.$this->container->get("frontend_manager")->blocks()[$block_name]["block"];
            $block = new $block_class($block_name, $settings);
            $block->block_name = $block->settings["block_name"] = $block_name;
        }

        if ($block->settings["id"] != "") {
            $this->settings["blocks"][$block->settings["id"]] = $block;
        } else {
            $this->settings["blocks"][] = $block;
        }

        return $this;
    }

    /**
     * @param $block_name
     * @param array $settings
     * @return string
     */
    public function &withBlock($block_name, $settings = [])
    {

        if (is_array($block_name)) {
            $settings = $block_name;
            $block_name = "block";
        }

        if (!is_string($block_name)) {
            $block = $block_name;
            $block->parent_block = $this;
        } else {
            $block_class = 'C4\FrontendBundle\Blocks\\'.$this->container->get("frontend_manager")->blocks()[$block_name]["block"];
            $block = new $block_class($block_name, $settings, $this);
            $block->block_name = $block->settings["block_name"] = $block_name;
        }

        if ($block->settings["id"] != "") {
            $this->settings["blocks"][$block->settings["id"]] = $block;
        } else {
            $this->settings["blocks"][] = $block;
        }

        return $block;
    }

    /**
     * @return null
     * returns parent block if have
     */
    public function endWith()
    {
        return $this->parent_block;
    }

    /**
     * @param bool $return_as_response
     * @return string|Response
     */
    public function render($return_as_response = false)
    {
        if (!$this->settings["render"]) {
            $html = "";
        } else {

            $html = $this->container->get('twig')->render(
                $this->template,
                array_merge(
                    [
                        "this" => $this,
                    ],
                    $this->settings
                )
            );
        }

        // we can return block results directly as response
        if ($return_as_response) {
            $response = new Response();
            $response->setContent($html);

            return $response;
        } else {
            return $html;
        }
    }

    /**
     * @param $id
     * @return BaseBlock
     */
    public function findBlock($id)
    {
        if (!isset($this->settings["blocks"][$id])) {
            return new self(
                "block", [
                    "id"     => $id,
                    "render" => false,
                ]
            );
        }

        return $this->settings["blocks"][$id];
    }

    // alias
    public function getBlock($id)
    {
        return $this->findBlock($id);
    }

    // alias
    public function block($id)
    {
        return $this->findBlock($id);
    }

    /**
     * @param $key
     * @return mixed|string
     */
    public function get($key)
    {
        return (isset($this->settings[$key])) ? $this->settings[$key] : "";
    }

}